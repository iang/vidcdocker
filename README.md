# Linux videoconferencing docker image

On the assumption that there will be a lot more videoconferencing going
on in the near future, I put together a docker image containing skype,
zoom, and wire, so that you can run them inside a container and have a
little less worry about the random binaries.

## Dependencies

I made this to run on Ubuntu 18.04, but you may have luck with other
platforms as well.

  - docker
    - If you don't already have docker installed,
      [install Docker desktop](https://www.docker.com/get-started) first.

  - x11docker
    - `git clone https://github.com/mviereck/x11docker`
    - copy `x11docker/x11docker` into some directory in your $PATH

  - nxagent
    - `apt install nxagent`
    - There are other possible virtual X servers you could use here; see
      x11docker above for a list.  This is the part that's most
      sensitive to your host OS and version.

## Download skype and zoom

  - `./download-debs`

## Build the vidcdocker image

  - `./build-docker`

## Install the launch script

  - Copy `vidcdocker` somewhere in your $PATH

## Running vidcdocker

  - `vidcdocker` will start the docker image and launch an xterminal in
    that docker.  Your webcam and pulseaudio will be connected to that
    xterminal.  From that terminal, you can run `wire-desktop`,
    `skypeforlinux`, or `zoom`.  Exiting the terminal will terminate the
    docker container.
